<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp_sample');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'wordpress');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', '%W0rdpr355_');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';l1~.1hX)g|U<RBtrR^m!L[}}I$<MV<gb JX|0g|-G7yVzdE&5|92d;5M_s3D%D+');
define('SECURE_AUTH_KEY',  'F23k(dM!`?)vm66n|h,QF#H%SyB-c]A6x{;/R>XWj2ot4PItx>IB9gEP4[%|+80R');
define('LOGGED_IN_KEY',    '-YlcV_IcF0&CJ0g~K.|3<G|<vRY6KAg{cL/mYAQsP9a^a>?a!j_;V-Ji-#b?< t%');
define('NONCE_KEY',        'cV(=CQruK]itZL9|,dh@%}(CVer_HoLEXf`%WE@PBf8L7za5@~# |Sak)1siAxqb');
define('AUTH_SALT',        'Es37a#@xH[J;?b01Qp{VI22gF9 =,,O;r.:w*g0q]r@C4>&s%[RJ%R.I[%^K&s)]');
define('SECURE_AUTH_SALT', 'n+]kzw%*jG-xiLF_ZF5[5mqct^J=o3?/M_W@k-?3gWSI%KI+EYDyh?&ctdnMUCsd');
define('LOGGED_IN_SALT',   '5jjl-2=^0_otJ_2HM,+ptYMbruL+knK3fU;:eood)W&?D~+VIto3gIFpw/N$ b;#');
define('NONCE_SALT',       'T}l:`G3k`|NpK?,V*@dj9T{Cm)g?=L>#:ewRQJ.wp#CXAK)B(|sYyKU5WP.o:)kf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
